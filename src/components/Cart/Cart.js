import React from 'react';
import {connect} from "react-redux";
import {Button, Col, FormControl, Modal, Panel, Row} from "react-bootstrap";
import {deleteItemCard, sendCart} from "../../store/cart-action";

const mapStateToProps = state => {
  return {
    cart: state.cartReducer.cart,
    dishes: state.dishesReducer.dishes
  };
};

const mapDispatchToProps = dispatch => {
  return {
    deleteItemCart: (dishes) => dispatch(deleteItemCard(dishes)),
    sendCart: (cart) => dispatch(sendCart(cart))
  };
};

class Cart extends React.Component {
  state = {
    modalShow: false
  };

  render() {
    return (
      <Panel>
        <Panel.Heading>
          Cart
        </Panel.Heading>
        <Panel.Body>
          {Object.keys(this.props.cart).map((key, index) => <div
            key={index}
            onClick={() => this.props.deleteItemCart(key)}>
            <Row>
              <Col xs={6}>
                {key}
              </Col>
              <Col xs={2} className={'text-center'}>
                <b>{this.props.cart[key]}</b>
              </Col>
              <Col xs={4}>
                <span>{this.props.dishes[index].cost * this.props.cart[key]}</span> Руб.
              </Col>
            </Row>
          </div>)}
        </Panel.Body>
        <Panel.Footer>
          <Button onClick={() => {this.setState({modalShow: true})}} disabled={Object.keys(this.props.cart).length > 0 ? false : true}>Place order</Button>
          <div hidden={!this.state.modalShow} className="static-modal">
            <Modal.Dialog>
              <Modal.Header>
                <Modal.Title>Order</Modal.Title>
              </Modal.Header>
              <Modal.Body>
                <FormControl
                  type="text"
                  placeholder="Name"
                />
                <FormControl
                  type="text"
                  placeholder="Email"
                />
                <FormControl
                  type="text"
                  value={this.state.value}
                  placeholder="Tel number"
                />
              </Modal.Body>
              <Modal.Footer>
                <Button onClick={() => {this.setState({modalShow: false})}}>Close</Button>
                <Button onClick={() => {this.props.sendCart(this.props.cart); this.setState({modalShow: false})}} bsStyle="primary">Ordered</Button>
              </Modal.Footer>
            </Modal.Dialog>
          </div>
        </Panel.Footer>
      </Panel>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Cart);