import React from 'react';
import {connect} from "react-redux";
import {fetchDishes} from "../../store/dishes-action";
import {Button, Image, Panel} from "react-bootstrap";
import {addToCard} from "../../store/cart-action";

const mapStateToProps = state => {
  return {
    dishes: state.dishesReducer.dishes,
  }
};

const mapDispatchToProps = dispatch => {
  return {
    fetchDishes: () => dispatch(fetchDishes()),
    addToCard: (dishes) => dispatch(addToCard(dishes))
  }
};

class Dishes extends React.Component {

  componentDidMount() {
    this.props.fetchDishes();
  }

  render() {
    console.log(this.props.dishes);
    return (
      <div>
        {this.props.dishes.map((value, index) =>
          <Panel key={index}>
            <Panel.Heading>
              <Image style={{maxWidth: '120px'}} src={value.img} circle />{value.name}
            </Panel.Heading>
            <Panel.Body>
              {value.cost} Руб. <Button className={'pull-right'} onClick={() => this.props.addToCard(value.name)}>Add to cart</Button>
            </Panel.Body>
          </Panel>
        )}
      </div>
    )
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Dishes);