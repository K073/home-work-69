import axios from 'axios';

const instance = axios.create({
  baseURL: 'https://home-work-69.firebaseio.com'
});

export default instance;