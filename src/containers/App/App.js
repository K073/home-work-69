import React, { Component } from 'react';
import Dishes from "../../components/Dishes/Dishes";
import {Col, Grid, Row} from "react-bootstrap";
import Cart from "../../components/Cart/Cart";

class App extends Component {
  render() {
    return (
      <div className="App">
        <Grid>
          <Row>
            <Col md={8}>
              <Dishes/>
            </Col>
            <Col md={4}>
              <Cart/>
            </Col>
          </Row>
        </Grid>
      </div>
    );
  }
}

export default App;
