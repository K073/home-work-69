import {FETCH_DISHES_SUCCESS} from "./dishes-action";

const initialState = {
  dishes: []
};

const dishes = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_DISHES_SUCCESS:
      return {...state, dishes: action.data};
    default:
      return state;
  }
};

export default dishes;
