import {ADD_TO_CARD, DELETE_ITEM_CARD} from "./cart-action";

const initialState = {
  cart: {}
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_TO_CARD:
      const cart = {...state.cart};
      cart[action.dishes] = cart[action.dishes] ? cart[action.dishes] + 1 : 1;
      return {...state, cart};
    case DELETE_ITEM_CARD:
      const cartRemoved = {...state.cart};
      delete cartRemoved[action.dishes];
      return {...state, cart: cartRemoved};
    default:
      return state;
  }
};

export default reducer;
