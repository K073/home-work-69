import axios from '../axios';

export const ADD_TO_CARD = 'ADD_TO_CARD';
export const DELETE_ITEM_CARD = 'DELETE_ITEM_CARD';

export const SEND_CART_REQUEST = 'SEND_CART_REQUEST';
export const SEND_CART_SUCCESS = 'SEND_CART_SUCCESS';
export const SEND_CART_ERROR = 'SEND_CART_ERROR';

export const addToCard = dishes => {
  return {type: ADD_TO_CARD, dishes}
};

export const deleteItemCard = dishes => {
  return {type: DELETE_ITEM_CARD, dishes}
};


export const sendCartRequest = () => {
  return { type: SEND_CART_REQUEST };
};

export const sendCartSuccess = () => {
  return { type: SEND_CART_SUCCESS};
};

export const sendCartError = () => {
  return { type: SEND_CART_ERROR };
};

export const sendCart = (cart) => {
  return dispatch => {
    dispatch(sendCartRequest());
    axios.post('/orders.json', cart).then(response => {
      dispatch(sendCartSuccess());
    }, error => {
      dispatch(sendCartError());
    });
  }
};

