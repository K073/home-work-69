import {combineReducers} from "redux";

import dishesReducer from './dishes-reducer';
import cartReducer from './cart-reducer';

export default combineReducers({
  dishesReducer,
  cartReducer
})