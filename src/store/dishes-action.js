import axios from '../axios'

export const FETCH_DISHES_REQUEST = 'FETCH_DISHES_REQUEST';
export const FETCH_DISHES_SUCCESS = 'FETCH_DISHES_SUCCESS';
export const FETCH_DISHES_ERROR = 'FETCH_DISHES_ERROR';


export const fetchDishesRequest = () => {
  return { type: FETCH_DISHES_REQUEST };
};

export const fetchDishesSuccess = (data) => {
  return { type: FETCH_DISHES_SUCCESS, data};
};

export const fetchDishesError = () => {
  return { type: FETCH_DISHES_ERROR };
};

export const fetchDishes = () => {
  return dispatch => {
    dispatch(fetchDishesRequest());
    axios.get('/dishes.json').then(response => {
      dispatch(fetchDishesSuccess(response.data));
    }, error => {
      dispatch(fetchDishesError());
    });
  }
};